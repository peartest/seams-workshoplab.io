---
fullname: Firstname Lastname
goby: Personalname
img: yourface.jpg
links:
  -
    title: some link
    url: https://to.your.stuff/
  -
    title: another
    url: https://link.to.things/
affiliation:
  -
    org: Some Company
    position: Junior Dev.
---